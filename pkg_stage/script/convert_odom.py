#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose, Quaternion
from geometry_msgs.msg import PoseWithCovariance 
import sys, select, termios, tty
import math 

a =  0.75
b=  0.4

def callback(data):

    odom = data
    odom.pose.pose.position.x =  data.pose.pose.position.x
    odom.pose.pose.position.y = data.pose.pose.position.y
    #odom.pose.pose.position.z = data.pose.pose.position.z
    #odom.pose.pose.orientation.x = a*data.pose.pose.orientation.x 
    #odom.pose.pose.orientation.y = data.pose.pose.orientation.y 
    #odom.pose.pose.orientation.z = -data.pose.pose.orientation.z 
    #odom.pose.pose.orientation.w = -data.pose.pose.orientation.w
    pub.publish(odom)


if __name__=="__main__":

    rospy.init_node("convert_odom")
    pub = rospy.Publisher('odometry/convert', Odometry, queue_size=5)

    rospy.Subscriber("odometry/filtered", Odometry, callback)
    rospy.spin()
