#!/usr/bin/env python



import rospy

from sensor_msgs.msg import NavSatFix
from geographic_msgs.msg import GeoPoint
from    mavros_msgs.msg import  HilGPS

import sys, select, termios, tty


def callback(data):
    gps= HilGPS ()
    gps.geo.latitude = data.latitude
    gps.geo.longitude = data.longitude
    gps.geo.altitude = data.altitude
    gps.header.frame_id="map"
    gps.satellites_visible=10
    gps.fix_type=5
    gps.epv=10
    gps.vel=2
    gps.vn=3
    gps.ve=2
    gps.vd=10
    pub.publish(gps)

if __name__=="__main__":
    #/gps/filtered /mavros/global_position/raw/fix
    rospy.init_node("convert_gps")


    pub = rospy.Publisher("/mavros/hil/gps", HilGPS, queue_size=5)
    rospy.Subscriber("/gps/filtered", NavSatFix, callback)
    rospy.spin()
